#!/bin/bash

sudo apt-get install python-django --assume-yes # install django

sudo apt-get install git --assume-yes # install git
cd /home/vagrant
git clone https://core4quad@bitbucket.org/core4quad/projekt_zespolowy_2.git # clone from repository

cd /home/vagrant/projekt_zespolowy_2/Django
python manage.py runserver [::]:8000 # run django server