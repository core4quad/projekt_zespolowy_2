from django.contrib import admin
from .models import Creative, Impression, Click


for model in Creative, Impression, Click:
    admin.site.register(model)