import uuid
from django.db import models



class Creative(models.Model):
    """
    Table keeps information about adverts.
    :param Name: name of advert
    :param Width: width of advert
    :param Height: height of advert
    :param Path: path of file with advert
    :param Redirect: page of redirect (max length URL is 2083)
    """
    ID = models.AutoField(primary_key=True)
    """ID: Primary Key (created automatically)"""
    Name = models.CharField(max_length=160)
    Width = models.IntegerField()
    Height = models.IntegerField()
    Path = models.CharField(max_length=160)
    Redirect = models.CharField(max_length=2083)
    def __str__(self):
        return '%s %s %s %s %s' % (self.Name, self.Width, self.Height, self.Path, self. Redirect)


class Impression(models.Model):
    """
    Table keeps information about impressions of adverts on pages.
    :param Creative: Foreign Key (to Creative table)
    :param Date: date of impression to advert
    :param Cookie_ID: UUID4 of visitor
    """
    ID = models.AutoField(primary_key=True)
    """ID: Primary Key (created automatically)"""
    Creative = models.ForeignKey(Creative, on_delete=models.CASCADE)
    Date = models.DateField()
    Cookie_ID = models.UUIDField(default=uuid.uuid4)
    def __str__(self):
        return '%s %s' % (self.Date, self.Cookie_ID)


class Click(models.Model):
    """
    Table keeps information about clicks adverts on pages.
    :param Creative: Foreign Key (to Creative table)
    :param Date: date of click on advert
    :param Cookie_ID: UUID4 of visitor
    """
    ID = models.AutoField(primary_key=True)
    """ID: Primary Key (created automatically)"""
    Creative = models.ForeignKey(Creative, on_delete=models.CASCADE)
    Date = models.DateField()
    Cookie_ID = models.UUIDField(default=uuid.uuid4)
    def __str__(self):
        return '%s %s' % (self.Date, self.Cookie_ID)
