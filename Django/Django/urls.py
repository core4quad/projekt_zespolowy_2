from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^Adserver/', include('Adserver.urls')),
    url(r'^admin/', admin.site.urls),
]
